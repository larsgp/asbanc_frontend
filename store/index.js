// import Cookies from 'universal-cookies';

export const state = () => ({
    tokenAuth:'',    
    username:'',
    pass:'',
    isOpenSearch:false,
    FootHead:'',
    SeoG:[]
  })
  
  export const mutations = {
    savingToken(state,text){
      state.tokenAuth = text;
    },
    openSearchBox(state,text){
        state.isOpenSearch = text;
    },      
    UsernameEmpresa(state,text){
      state.username = text;
    },
    PassEmpresa(state,text){
      state.pass = text;
    },
    FooterHeader(state,FootHead){
      state.FootHead = FootHead
    },
    SeoGeneral(state,seo){
      state.SeoG = seo
    }
  }
  
  export const actions = {
    async getFooterHeaderData ({ commit }) {
      const data = await this.$axios.$get(`/header-footer/`)
      commit('FooterHeader', data[0])
    },
    async getGeneralSeo ({ commit }) {
      const data = await this.$axios.$get(`/seo/`)
      commit('SeoGeneral', data)
    }
  }