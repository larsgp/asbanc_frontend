import { extend } from "vee-validate";
// import { required, alpha } from "vee-validate/dist/rules";
import * as rules from 'vee-validate/dist/rules';
import { messages } from 'vee-validate/dist/locale/es.json';

Object.keys(rules).forEach(rule => {
    extend(rule, {
      ...rules[rule], // copies rule configuration
      message: messages[rule] // assign message
    });
  });

// extend("required", {
//   ...required,
//   message: "Este campo es requerido"
// });

// extend("alpha", {
//   ...alpha,
//   message: "Solo se aceptan caracteres alfabeticos"
// });
// extend("number", {
//     ...alpha,
//     message: "Solo se aceptan caracteres númericos"
//   });

extend('cellphone', {
    validate: value => value === 'example',
    message: 'This is not the magic word'
  });