require('dotenv').config()
module.exports = {
  loading: '@/components/Loader.vue',
  // Global page headers (https://go.nuxtjs.dev/config-head)
  head: {
    title: 'asbanc',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  // Global CSS (https://go.nuxtjs.dev/config-css)
  css: [
    '@/assets/css/main.scss',
    'video.js/dist/video-js.css'
  ],
  //buildDir: 'nuxt-dist',
  // generate: {    
  //   routes: function () {
  //     return axios.get(
  //       process.env.SERVER
  //     )
  //     .then((response) => {
  //         let users = response.data.map((user) => {
  //             return {
  //               route: '/users/' + user.id,
  //               payload: user
  //             }
  //         });
  //         return ['/some-other-dynamic-route-or-array-of-routes/', ...users]
  //      });
  //    }
  // },
  // Plugins to run before rendering page (https://go.nuxtjs.dev/config-plugins)
  plugins: [
    { src: '@/plugins/nuxt-swiper-plugin.js', mode: 'client' },
    { src: '@/plugins/vue-slick-carousel.js' },
    { src: '@/plugins/nuxt-video-player-plugin.js', ssr: false },
    { src: '@/plugins/axios.js' },
    { src: '@/plugins/vee-validate.js' },
    {src: '@/plugins/vue-cookie-law.js', mode: 'client'}
  ],

  // Auto import components (https://go.nuxtjs.dev/config-components)
  components: true,

  // Modules for dev and build (recommended) (https://go.nuxtjs.dev/config-modules)
  buildModules: [
    // other build modules
    '@nuxtjs/moment',
  ],
  moment: {
    defaultLocale: 'es',
    locales: ['es']
  },
  // Modules (https://go.nuxtjs.dev/config-modules)
  modules: [
    // https://go.nuxtjs.dev/bootstrap
    'bootstrap-vue/nuxt',
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',        
    '@nuxtjs/dotenv',
    '@nuxtjs/sitemap',
    'vue-social-sharing/nuxt',
    '@nuxtjs/recaptcha'
  ],
  recaptcha: {
    hideBadge: false,
    siteKey: '6LcV1hkaAAAAAJCbKAWGCl8sG5vzk-WKF7O9opEL',
    version: 2,
    size: 'normal' // Size: 'compact', 'normal', 'invisible' 
  },
  bootstrapVue: {
    icons: true
  },
  generate: {    
  },
  sitemap: {
    hostname: 'https://desa01.asbsis.com',
    gzip: true,
    exclude: [
      '/secret',
      '/admin/**'
    ],
    routes: [
      'educacion-financiera',
      'usuarios',
      'servicios',
      'servicio',
      'estadisticas-del-sector',      
      'noticia',  
      'nuestra-asociacion',

    ]
  },
  router: {    
    extendRoutes(routes, resolve) {
      routes.push({
        name: 'custom',
        path: '*',
        component: resolve(__dirname, 'pages/404.vue')
      })
    }
  },
  // server: {
  //   port: 80, // default: 3000
  //   host: 'localhost' // default: localhost
  // },
  // Axios module configuration (https://go.nuxtjs.dev/config-axios)
  babel: { compact: true },
  env: {
    baseUrl: process.env.BASE_URL || 'http://localhost:3000',
    backUrl: process.env.BACK_URL
  },
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
    baseURL: process.env.SERVER,
    proxyHeaders: false,
    credentials: false,
    https:false
  },
  // Build Configuration (https://go.nuxtjs.dev/config-build)
  build: {
    transpile: ["vee-validate/dist/rules"],
  },
  proxy: {
    '/api/': { target: process.env.BACK_URL, pathRewrite: {'^/api/': ''}, changeOrigin: true }
  }
  // publicRuntimeConfig: {
  //   baseURL: process.env.SERVER || 'http://localhost:8000/api'
  // }
}
