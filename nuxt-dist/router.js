import Vue from 'vue'
import Router from 'vue-router'
import { normalizeURL, decode } from '@nuxt/ufo'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _60fdd415 = () => interopDefault(import('..\\pages\\404.vue' /* webpackChunkName: "pages/404" */))
const _6fbe8242 = () => interopDefault(import('..\\pages\\contacto.vue' /* webpackChunkName: "pages/contacto" */))
const _f9e724c6 = () => interopDefault(import('..\\pages\\educacion-financiera.vue' /* webpackChunkName: "pages/educacion-financiera" */))
const _7c781985 = () => interopDefault(import('..\\pages\\estadisticas-del-sector.vue' /* webpackChunkName: "pages/estadisticas-del-sector" */))
const _792dcf5b = () => interopDefault(import('..\\pages\\eventos.vue' /* webpackChunkName: "pages/eventos" */))
const _36942b93 = () => interopDefault(import('..\\pages\\glosario.vue' /* webpackChunkName: "pages/glosario" */))
const _a74f49c4 = () => interopDefault(import('..\\pages\\libro-de-reclamaciones.vue' /* webpackChunkName: "pages/libro-de-reclamaciones" */))
const _472887f5 = () => interopDefault(import('..\\pages\\nuestra-asociacion.vue' /* webpackChunkName: "pages/nuestra-asociacion" */))
const _443bb42f = () => interopDefault(import('..\\pages\\politicas-de-privacidad.vue' /* webpackChunkName: "pages/politicas-de-privacidad" */))
const _8dec8036 = () => interopDefault(import('..\\pages\\preguntas-frecuentes.vue' /* webpackChunkName: "pages/preguntas-frecuentes" */))
const _db01ad28 = () => interopDefault(import('..\\pages\\prensa.vue' /* webpackChunkName: "pages/prensa" */))
const _813c1a0c = () => interopDefault(import('..\\pages\\servicios.vue' /* webpackChunkName: "pages/servicios" */))
const _5f56987f = () => interopDefault(import('..\\pages\\simuladores.vue' /* webpackChunkName: "pages/simuladores" */))
const _376034c1 = () => interopDefault(import('..\\pages\\terminos-y-condiciones.vue' /* webpackChunkName: "pages/terminos-y-condiciones" */))
const _15c649f8 = () => interopDefault(import('..\\pages\\usuarios.vue' /* webpackChunkName: "pages/usuarios" */))
const _13619e0b = () => interopDefault(import('..\\pages\\educacion_financiera\\_slug.vue' /* webpackChunkName: "pages/educacion_financiera/_slug" */))
const _1031dc50 = () => interopDefault(import('..\\pages\\estadistica-del-sector\\_slug.vue' /* webpackChunkName: "pages/estadistica-del-sector/_slug" */))
const _84bf511c = () => interopDefault(import('..\\pages\\evento\\_slug.vue' /* webpackChunkName: "pages/evento/_slug" */))
const _54969a18 = () => interopDefault(import('..\\pages\\noticia\\_slug.vue' /* webpackChunkName: "pages/noticia/_slug" */))
const _3c25f05e = () => interopDefault(import('..\\pages\\servicio\\_slug.vue' /* webpackChunkName: "pages/servicio/_slug" */))
const _9cb0af62 = () => interopDefault(import('..\\pages\\usuario\\_slug.vue' /* webpackChunkName: "pages/usuario/_slug" */))
const _03e28eda = () => interopDefault(import('..\\pages\\webinar\\_slug.vue' /* webpackChunkName: "pages/webinar/_slug" */))
const _3c8ed96f = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages/index" */))

// TODO: remove in Nuxt 3
const emptyFn = () => {}
const originalPush = Router.prototype.push
Router.prototype.push = function push (location, onComplete = emptyFn, onAbort) {
  return originalPush.call(this, location, onComplete, onAbort)
}

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: '/',
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/404",
    component: _60fdd415,
    name: "404"
  }, {
    path: "/contacto",
    component: _6fbe8242,
    name: "contacto"
  }, {
    path: "/educacion-financiera",
    component: _f9e724c6,
    name: "educacion-financiera"
  }, {
    path: "/estadisticas-del-sector",
    component: _7c781985,
    name: "estadisticas-del-sector"
  }, {
    path: "/eventos",
    component: _792dcf5b,
    name: "eventos"
  }, {
    path: "/glosario",
    component: _36942b93,
    name: "glosario"
  }, {
    path: "/libro-de-reclamaciones",
    component: _a74f49c4,
    name: "libro-de-reclamaciones"
  }, {
    path: "/nuestra-asociacion",
    component: _472887f5,
    name: "nuestra-asociacion"
  }, {
    path: "/politicas-de-privacidad",
    component: _443bb42f,
    name: "politicas-de-privacidad"
  }, {
    path: "/preguntas-frecuentes",
    component: _8dec8036,
    name: "preguntas-frecuentes"
  }, {
    path: "/prensa",
    component: _db01ad28,
    name: "prensa"
  }, {
    path: "/servicios",
    component: _813c1a0c,
    name: "servicios"
  }, {
    path: "/simuladores",
    component: _5f56987f,
    name: "simuladores"
  }, {
    path: "/terminos-y-condiciones",
    component: _376034c1,
    name: "terminos-y-condiciones"
  }, {
    path: "/usuarios",
    component: _15c649f8,
    name: "usuarios"
  }, {
    path: "/educacion_financiera/:slug?",
    component: _13619e0b,
    name: "educacion_financiera-slug"
  }, {
    path: "/estadistica-del-sector/:slug?",
    component: _1031dc50,
    name: "estadistica-del-sector-slug"
  }, {
    path: "/evento/:slug?",
    component: _84bf511c,
    name: "evento-slug"
  }, {
    path: "/noticia/:slug?",
    component: _54969a18,
    name: "noticia-slug"
  }, {
    path: "/servicio/:slug?",
    component: _3c25f05e,
    name: "servicio-slug"
  }, {
    path: "/usuario/:slug?",
    component: _9cb0af62,
    name: "usuario-slug"
  }, {
    path: "/webinar/:slug?",
    component: _03e28eda,
    name: "webinar-slug"
  }, {
    path: "/",
    component: _3c8ed96f,
    name: "index"
  }, {
    path: "*",
    component: _60fdd415,
    name: "custom"
  }],

  fallback: false
}

function decodeObj(obj) {
  for (const key in obj) {
    if (typeof obj[key] === 'string') {
      obj[key] = decode(obj[key])
    }
  }
}

export function createRouter () {
  const router = new Router(routerOptions)

  const resolve = router.resolve.bind(router)
  router.resolve = (to, current, append) => {
    if (typeof to === 'string') {
      to = normalizeURL(to)
    }
    const r = resolve(to, current, append)
    if (r && r.resolved && r.resolved.query) {
      decodeObj(r.resolved.query)
    }
    return r
  }

  return router
}
