export { default as Loading } from '../..\\components\\Loading.vue'
export { default as Logo } from '../..\\components\\Logo.vue'
export { default as Banners } from '../..\\components\\Globals\\Banners.vue'
export { default as CardBox } from '../..\\components\\Globals\\CardBox.vue'
export { default as CardsBox } from '../..\\components\\Globals\\CardsBox.vue'
export { default as CardsBoxToggle } from '../..\\components\\Globals\\CardsBoxToggle.vue'
export { default as FooterPage } from '../..\\components\\Globals\\FooterPage.vue'
export { default as InformationTemplates } from '../..\\components\\Globals\\informationTemplates.vue'
export { default as Navbar } from '../..\\components\\Globals\\Navbar.vue'
export { default as NewsTemplates } from '../..\\components\\Globals\\newsTemplates.vue'
export { default as SearchBox } from '../..\\components\\Globals\\searchBox.vue'
export { default as ServicesCards } from '../..\\components\\Globals\\ServicesCards.vue'
export { default as SocialFloat } from '../..\\components\\Globals\\social-float.vue'
export { default as VideoPlayer } from '../..\\components\\Globals\\VideoPlayer.vue'
export { default as Whatsapp } from '../..\\components\\Globals\\whatsapp.vue'

export const LazyLoading = import('../..\\components\\Loading.vue' /* webpackChunkName: "components_Loading" */).then(c => c.default || c)
export const LazyLogo = import('../..\\components\\Logo.vue' /* webpackChunkName: "components_Logo" */).then(c => c.default || c)
export const LazyBanners = import('../..\\components\\Globals\\Banners.vue' /* webpackChunkName: "components_Globals/Banners" */).then(c => c.default || c)
export const LazyCardBox = import('../..\\components\\Globals\\CardBox.vue' /* webpackChunkName: "components_Globals/CardBox" */).then(c => c.default || c)
export const LazyCardsBox = import('../..\\components\\Globals\\CardsBox.vue' /* webpackChunkName: "components_Globals/CardsBox" */).then(c => c.default || c)
export const LazyCardsBoxToggle = import('../..\\components\\Globals\\CardsBoxToggle.vue' /* webpackChunkName: "components_Globals/CardsBoxToggle" */).then(c => c.default || c)
export const LazyFooterPage = import('../..\\components\\Globals\\FooterPage.vue' /* webpackChunkName: "components_Globals/FooterPage" */).then(c => c.default || c)
export const LazyInformationTemplates = import('../..\\components\\Globals\\informationTemplates.vue' /* webpackChunkName: "components_Globals/informationTemplates" */).then(c => c.default || c)
export const LazyNavbar = import('../..\\components\\Globals\\Navbar.vue' /* webpackChunkName: "components_Globals/Navbar" */).then(c => c.default || c)
export const LazyNewsTemplates = import('../..\\components\\Globals\\newsTemplates.vue' /* webpackChunkName: "components_Globals/newsTemplates" */).then(c => c.default || c)
export const LazySearchBox = import('../..\\components\\Globals\\searchBox.vue' /* webpackChunkName: "components_Globals/searchBox" */).then(c => c.default || c)
export const LazyServicesCards = import('../..\\components\\Globals\\ServicesCards.vue' /* webpackChunkName: "components_Globals/ServicesCards" */).then(c => c.default || c)
export const LazySocialFloat = import('../..\\components\\Globals\\social-float.vue' /* webpackChunkName: "components_Globals/social-float" */).then(c => c.default || c)
export const LazyVideoPlayer = import('../..\\components\\Globals\\VideoPlayer.vue' /* webpackChunkName: "components_Globals/VideoPlayer" */).then(c => c.default || c)
export const LazyWhatsapp = import('../..\\components\\Globals\\whatsapp.vue' /* webpackChunkName: "components_Globals/whatsapp" */).then(c => c.default || c)
