if (process.env.NODE_ENV !== 'production') {
  require('dotenv').config();
}
const { loadNuxt, build } = require('nuxt')

const app = require('express')()
const isDev = process.env.NODE_ENV !== 'production'
const port = process.env.PORT || 8700

async function start() {
  // We get Nuxt instance
  const nuxt = await loadNuxt(isDev ? 'start' : 'dev')

  // Render every route with Nuxt.js
  app.use(nuxt.render)

  // Build only in dev mode with hot-reloading
  // if (isDev) {
    build(nuxt)
  // }
  // Listen the server
  app.listen(port, 'localhost')
  console.log('Server listening on `localhost:' + port + '`. and Mode: '+process.env.NODE_ENV)  
}

start()